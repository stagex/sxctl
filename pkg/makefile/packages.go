package makefile

import (
	"fmt"
	"io"
	"text/template"

	"codeberg.org/stagex/sxctl/pkg/library"
)

// GeneratePackagesMk generates the `packages.mk` file contents from the given library contents.
func GeneratePackagesMk(out io.Writer, l library.Library) error {
	packageList, err := l.List()
	if err != nil {
		return fmt.Errorf("failed to get list of packages from library: %w", err)
	}

	funcMap := template.FuncMap{
		"hasDependencies": func(pkgIndex int) bool {
			return len(packageList[pkgIndex].Dependencies) > 0
		},
		"notLast": func(pkgIndex, depIndex int) bool {
			return depIndex+1 < len(packageList[pkgIndex].Dependencies)
		},
	}

	tmpl, err := template.New("packages").Funcs(funcMap).Parse(packagesTemplate)
	if err != nil {
		return fmt.Errorf("failed to parse packages template: %w", err)
	}

	return tmpl.Execute(out, packageList)
}
