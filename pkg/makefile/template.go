package makefile

var packagesTemplate = `{{ range $packageIndex, $pkg := . }}
.PHONY: {{ $pkg.Name}}
{{ $pkg.Name }}: out/{{ $pkg.Name }}/index.json
out/{{ $pkg.Name }}/index.json: \
	packages/{{ $pkg.Name }}/Containerfile{{ if hasDependencies $packageIndex }} \{{ end }}
	{{ range $depIndex, $dep := $pkg.Dependencies -}}
	out/{{ $dep.Name }}/index.json{{ if notLast $packageIndex $depIndex }} \{{ end }}
	{{ end -}}
	$(call build,{{ $pkg.Name }})
{{ end }}
`
