package library_test

import (
	"strings"
	"testing"

	"codeberg.org/stagex/sxctl/pkg/library"
)

func TestItemContainerfileScan(t *testing.T) {
	expectedNames := []string{
		"bash",
		"binutils",
		"busybox",
		"gcc",
		"musl",
	}

	names, err := library.CollectDependencyNamesFromContainerFile("tests/Containerfile")
	if err != nil {
		t.Fail()
	}

	if len(names) != len(expectedNames) {
		t.Errorf("found incorrect number of dependency names (%d/%d): %s", len(names), len(expectedNames), strings.Join(names, ","))
	}

	for i, name := range expectedNames {
		if name != names[i] {
			t.Errorf("expected dependency %d to be %s; found %s", i, name, names[i])
		}
	}
}

func TestStage2Containerfile(t *testing.T) {
	expectedNames := []string{
		"stage1",
	}

	names, err := library.CollectDependencyNamesFromContainerFile("tests/stage2.Containerfile")
	if err != nil {
		t.Fail()
	}

	if len(names) != len(expectedNames) {
		t.Errorf("found incorrect number of dependency names (%d/%d): %s", len(names), len(expectedNames), strings.Join(names, ","))
	}

	for i, name := range expectedNames {
		if name != names[i] {
			t.Errorf("expected dependency %d to be %s; found %s", i, name, names[i])
		}
	}
}

func TestStage0Containerfile(t *testing.T) {
	names, err := library.CollectDependencyNamesFromContainerFile("tests/stage0.Containerfile")
	if err != nil {
		t.Fail()
	}

	if len(names) != 0 {
		t.Errorf("found incorrect number of dependency names (%d/%d): %s", len(names), 0, strings.Join(names, ","))
	}
}
