package library

import (
	"errors"
	"strings"
)

var ErrNotFound = errors.New("item not found in library")

type Library interface {
	// Add inserts an Item into the Library.
	Add(*Item) error

	// List returns the list of all Items in the Library.
	List() ([]*Item, error)

	// FindByName returns the Item from the Library which matches the given name, if it exists.
	// If it does not exist, an ErrNotFound will be returned.
	FindByName(name string) (*Item, error)
}

type ramLibrary []*Item

// NewRamLibrary returns a new RAM-based library.
func NewRamLibrary() Library {
	return &ramLibrary{}
}

func (l *ramLibrary) Add(i *Item) error {
	if _, err := l.FindByName(i.Name); errors.Is(err, ErrNotFound) {
		*l = append(*l, i)
	}

	return nil
}

func (l *ramLibrary) List() ([]*Item, error) {
	return *l, nil
}

func (l *ramLibrary) FindByName(name string) (*Item, error) {
	for _, item := range *l {
		if item.Name == strings.TrimPrefix(name, "stagex/") {
			return item, nil
		}
	}

	return nil, ErrNotFound
}
