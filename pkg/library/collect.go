package library

import (
	// CollectPackagesFromDirectory attempts to collect all packages located in the given directory into a common package Library.
	"errors"
	"fmt"
	"io/fs"
	"os"
	"path"
)

func CollectPackagesFromDirectory(baseDir string, l Library) error {
	addIfContainerFile := func(filePath string, dirEntry fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		if dirEntry.Name() == ContainerFileName {
			i, err := func() (*Item, error) {
				var sourcePath string = path.Join(baseDir, filePath)

				stat, err := os.Stat(sourcePath)
				if err != nil {
					return nil, fmt.Errorf("failed to open sourcePath %q: %w", sourcePath, err)
				}

				if !stat.IsDir() {
					return LoadItemFromPath(path.Dir(sourcePath))
				}

				if _, err := os.Stat(path.Join(sourcePath, ItemDescriptionFilename)); err != nil {
					if errors.Is(err, fs.ErrNotExist) {
						return LoadItemFromDefaults(sourcePath)
					}
					return nil, fmt.Errorf("failed to open %s for package in %q: %w", ItemDescriptionFilename, sourcePath, err)
				}

				return LoadItemFromStagexFile(sourcePath)
			}()
			if err != nil {
				return err
			}

			if err := l.Add(i); err != nil {
				return fmt.Errorf("failed to add package %q to library: %w", i.Name, err)
			}
		}

		return nil
	}

	if err := fs.WalkDir(os.DirFS(baseDir), ".", addIfContainerFile); err != nil {
		return fmt.Errorf("failed to search %q for packages: %w", baseDir, err)
	}

	list, err := l.List()
	if err != nil {
		return fmt.Errorf("failed to read list of packages from library: %w", err)
	}

	for _, i := range list {
		if err := i.DeriveDependencies(l); err != nil {
			return fmt.Errorf("failed to derive dependencies for package %q: %w", i.Name, err)
		}
	}

	return nil
}
