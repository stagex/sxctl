package library

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"io/fs"
	"os"
	"path"
	"regexp"
	"sort"
	"strings"
)

const (
	ContainerFileName       = "Containerfile"
	ItemDescriptionFilename = "stagex.json"
)

var (
	RegexFrom     = regexp.MustCompile(`^[Ff][Rr][Oo][Mm] stagex\/([^ :]+)`)
	RegexCopyFrom = regexp.MustCompile(`[Cc][Oo][Pp][Yy] --[Ff][Rr][Oo][Mm]=stagex\/([^ :]+)`)
)

// Item is a unique item inside the package library.
// It should compile to a single container image.
type Item struct {
	// Name is the unique name of the package.
	Name string `yaml:"name"`

	// Dependencies is the list of dependencies for this item.
	Dependencies []*Item `yaml:"dependencies"`

	// sourcePath is the directory in which the item definition was found.
	sourcePath string
}

// DeriveDependencies populates the dependencies of this package.
func (i *Item) DeriveDependencies(library Library) error {
	if i.Dependencies != nil {
		return nil
	}

	dependencyList, err := CollectDependencies(path.Join(i.sourcePath, ContainerFileName), library)
	if err != nil {
		return err
	}

	i.Dependencies = dependencyList

	return nil
}

// LoadItemFromPath loads or constructs an item from its source path.
// If a stagex.yaml exists, metadata for the Item will be loaded therefrom.
// Otherwise, defaults will be used.
func LoadItemFromPath(sourcePath string) (*Item, error) {
	stat, err := os.Stat(sourcePath)
	if err != nil {
		return nil, fmt.Errorf("failed to open sourcePath %q: %w", sourcePath, err)
	}

	// If this is not a directory, attempt to open the containing directory as the sourcePath instead.
	if !stat.IsDir() {
		return LoadItemFromPath(path.Dir(sourcePath))
	}

	if _, err := os.Stat(path.Join(sourcePath, ItemDescriptionFilename)); err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			return LoadItemFromDefaults(sourcePath)
		}

		return nil, fmt.Errorf("failed to open %s for package in %q: %w", ItemDescriptionFilename, sourcePath, err)
	}

	return LoadItemFromStagexFile(sourcePath)
}

// LoadItemFromStagexFile loads a package definition from a package definition file, rather than deriving the package metadata from the path.
func LoadItemFromStagexFile(src string) (*Item, error) {
	f, err := os.Open(src)
	if err != nil {
		return nil, fmt.Errorf("failed to read package definition file %q: %w", src, err)
	}

	defer f.Close() //nolint: errcheck

	i := new(Item)

	if err := json.NewDecoder(f).Decode(i); err != nil {
		return nil, fmt.Errorf("failed to parse package definition file %q: %w", src, err)
	}

	i.sourcePath = path.Dir(src)

	return i, nil
}

// LoadItemFromDefaults derives the package metadata from the given source path.
func LoadItemFromDefaults(sourcePath string) (*Item, error) {
	return &Item{
		Name:       path.Base(sourcePath),
		sourcePath: sourcePath,
	}, nil
}

// CollectDependencyNamesFromContainerFile reads from the given Containerfile, extracting the names of each dependency.
func CollectDependencyNamesFromContainerFile(containerFilePath string) (names []string, err error) {
	f, err := os.Open(containerFilePath)
	if err != nil {
		return nil, fmt.Errorf("failed to open %q: %w", containerFilePath, err)
	}

	defer f.Close() //nolint: errcheck

	appendIfNotExist := func(s string) {
		// ignore commented lines
		if strings.HasPrefix(s, "#") {
			return
		}

		// ignore duplicates
		for _, ref := range names {
			if ref == s {
				return
			}
		}

		names = append(names, s)
	}

	sc := bufio.NewScanner(f)

	for sc.Scan() {
		if matches := RegexFrom.FindStringSubmatch(sc.Text()); len(matches) > 1 {
			appendIfNotExist(matches[1])

			continue
		}

		if matches := RegexCopyFrom.FindStringSubmatch(sc.Text()); len(matches) > 1 {
			appendIfNotExist(matches[1])

			continue
		}
	}

	sort.Strings(names)

	return names, nil
}

// CollectDependencies derives the dependencies for a package.
func CollectDependencies(containerFilePath string, library Library) (dependencies []*Item, err error) {
	names, err := CollectDependencyNamesFromContainerFile(containerFilePath)
	if err != nil {
		return nil, err
	}

	for _, name := range names {
		i, err := library.FindByName(name)
		if err != nil {
			return nil, fmt.Errorf("failed to find dependency of %q in the library: %w", name, err)
		}

		dependencies = append(dependencies, i)
	}

	return dependencies, nil
}
