package library_test

import (
	"testing"

	"codeberg.org/stagex/sxctl/pkg/library"
)

func TestCollectPackagesFromDirectory(t *testing.T) {
	l := library.NewRamLibrary()

	if err := library.CollectPackagesFromDirectory("./tests/tree", l); err != nil {
		t.Error("failed to collect packages", err)
	}

	list, err := l.List()
	if err != nil {
		t.Error("failed to list packages:", err)
	}

	if len(list) != 3 {
		t.Errorf("unexpected package count: %d/%d", len(list), 3)
	}

	if len(list[2].Dependencies) != 1 {
		t.Errorf("unexpected dependency list length: %d/%d", len(list[2].Dependencies), 1)
	}

	item, err := l.FindByName("beta")
	if err != nil {
		t.Error("failed to find beta in library:", err)
	}

	if len(item.Dependencies) != 1 {
		t.Errorf("unexpected dependency list length: %d/%d", len(item.Dependencies), 1)
	}

	if item.Dependencies[0].Name != "alef" {
		t.Errorf("unexpected dependency name: %q; expected %q", item.Dependencies[0].Name, "alef")
	}
}
