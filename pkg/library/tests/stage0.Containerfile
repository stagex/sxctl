FROM alpine@sha256:51b67269f354137895d43f3b3d810bfacd3945438e94dc5ac55fdac340352f48 as alpine
FROM debian@sha256:bac353db4cc04bc672b14029964e686cd7bad56fe34b51f432c1a1304b9928da as debian
FROM archlinux@sha256:1f83ba0580a15cd6ad1d02d62ad432ddc940f53f07d0e39c8982d6c9c74e53e0 as arch

FROM scratch as base
ENV VERSION=1.6.0
ENV SRC_FILE=stage0-posix-${VERSION}.tar.gz
ENV SRC_SITE=https://github.com/oriansj/stage0-posix/releases/download/Release_${VERSION}/${SRC_FILE}
ENV SRC_HASH=9260ff69278366e5c056af7b8c436b74773eaa1330a0c6a6b8ab1b5f92e5065c
ADD --checksum=sha256:${SRC_HASH} ${SRC_SITE} .
COPY <<-EOF build.sh
	#!/bin/sh
	set -eux
	tar -xf ${SRC_FILE}
	cd stage0-posix-${VERSION}
	bootstrap-seeds/POSIX/x86/kaem-optional-seed
	mkdir -p /rootfs && cp -R * /rootfs/
	cd /rootfs
	sha256sum x86/bin/* > hashes.txt
    find /rootfs -exec touch -hcd "@0" "{}" +
EOF

FROM base as build1
COPY --from=debian . /
RUN apt update && apt install -y wget gcc
COPY --from=base . /
RUN --network=none sh build.sh

FROM base as build2
COPY --from=arch . /
RUN pacman -Sy --noconfirm wget gcc
COPY --from=base . /
RUN --network=none sh build.sh

FROM base as build3
COPY --from=alpine . /
RUN apk add wget gcc
COPY --from=base . /
RUN --network=none sh build.sh

FROM scratch as compare
COPY --from=build1 /rootfs/ /a
COPY --from=build2 /rootfs/ /b
COPY --from=build3 /rootfs/ /c

FROM compare as test1
WORKDIR /a
RUN ["x86/bin/sha256sum","-c","/a/hashes.txt"]
RUN ["x86/bin/sha256sum","-c","/b/hashes.txt"]
RUN ["x86/bin/sha256sum","-c","/c/hashes.txt"]

FROM compare as test2
WORKDIR /b
RUN ["x86/bin/sha256sum","-c","/a/hashes.txt"]
RUN ["x86/bin/sha256sum","-c","/b/hashes.txt"]
RUN ["x86/bin/sha256sum","-c","/c/hashes.txt"]

FROM compare as test3
WORKDIR /c
RUN ["x86/bin/sha256sum","-c","/a/hashes.txt"]
RUN ["x86/bin/sha256sum","-c","/b/hashes.txt"]
RUN ["x86/bin/sha256sum","-c","/c/hashes.txt"]

FROM scratch as install
COPY --from=test1 /a/hashes.txt /a-hashes.txt
COPY --from=test2 /b/hashes.txt /b-hashes.txt
COPY --from=test3 /c/hashes.txt /c-hashes.txt
COPY --from=build1 /rootfs /

FROM scratch as package
COPY --from=install / /
CMD ["x86/bin/kaem","--version"]
