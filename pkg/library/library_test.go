package library_test

import (
	"testing"

	"codeberg.org/stagex/sxctl/pkg/library"
)

func TestLibrary(t *testing.T) {
	l := library.NewRamLibrary()

	t.Run("add", func(t *testing.T) {
		origList, err := l.List()
		if err != nil {
			t.Fail()
		}

		origLen := len(origList)

		if err := l.Add(&library.Item{
			Name: "alpha",
		}); err != nil {
			t.Fail()
		}

		afterList, err := l.List()
		if err != nil {
			t.Fail()
		}

		if len(afterList) != origLen+1 {
			t.Fail()
		}
	})

	t.Run("deduplicate", func(t *testing.T) {
		origList, err := l.List()
		if err != nil {
			t.Fail()
		}

		origLen := len(origList)

		if err := l.Add(&library.Item{
			Name: "duplicate-test",
		}); err != nil {
			t.Fail()
		}

		if err := l.Add(&library.Item{
			Name: "duplicate-test",
		}); err != nil {
			t.Fail()
		}

		afterList, err := l.List()
		if err != nil {
			t.Fail()
		}

		if len(afterList) != origLen+1 {
			t.Fail()
		}
	})

	t.Run("FindByName", func(t *testing.T) {
		refItem := &library.Item{
			Name: "named-item",
		}

		if err := l.Add(refItem); err != nil {
			t.Fail()
		}

		foundItem, err := l.FindByName("named-item")
		if err != nil {
			t.Fail()
		}

		if foundItem.Name != refItem.Name {
			t.Fail()
		}
	})
}
