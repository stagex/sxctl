#!/bin/sh
go mod tidy

if [ -x $(which gofumpt 2>/dev/null) ]; then
   gofumpt -w .
else
   echo "NOTE: skipping gofumpt; please install it to use the extended formatting tool."
fi

if [ -x $(which gci 2>/dev/null) ]; then
   gci write --skip-generated -s standard -s default -s "Prefix(codeberg.org/stagex/sxctl)" .
else
   echo "NOTE: 'gci' is not installed.  Please install it to use the import organization tool."
   echo
fi

if [ -x $(which golangci-lint 2>/dev/null) ]; then
   golangci-lint run
else
   echo "NOTE: 'golangci-lint' is not installed.  Please install it to use the metalinter"
   echo
fi 
