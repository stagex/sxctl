# Stagex control tool

`sxctl` is a control tool for developers of the [StageX](https://stagex.tools) project.

Presently, the main command is `gen make`, which generates the packages makefile for stagex.

An example usage would be:

```sh
cd stagex
docker run --rm \
  --volume .:/src \
  --user $(id -u):$(id -g) \
  sxctl \
  -- -baseDir=/src gen make
```

It can also be compiled and used directly:

```sh
cd stagex
sxctl gen make
```

## Install-from-container

`sxctl` contains a function which allows itself to be easily copied out from its container.
To use it, bind mount the receiving directory to `/target` inside the container.
Something like this should do:

```sh
mkdir -p /home/myuser/.local/bin
docker run --rm \
  --volume /home/myuser/.local/bin:/target
  --user $(id -u):$(id -g) \
  sxctl install
```

