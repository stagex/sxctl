package main

import (
	"flag"
	"fmt"
	"log"
)

var baseDir string

func init() {
	flag.StringVar(&baseDir, "baseDir", ".", "base directory of the packages repository")
}

func main() {
	flag.Parse()

	if len(flag.Args()) < 1 {
		fmt.Println("no command supplied")
		fmt.Println("valid commands:")
		fmt.Println("  gen")

		flag.PrintDefaults()
		return
	}

	switch flag.Arg(0) {
	case "gen":
		mainGen()
	case "install":
		targetDirectory := "/target"
		if len(flag.Args()) > 1 {
			targetDirectory = flag.Arg(1)
		}

		if err := Install(targetDirectory); err != nil {
			log.Printf("failed to install to %q: %s", targetDirectory, err.Error())
		}
	default:
		log.Fatalf("unhandled primary command %q", flag.Arg(0))
	}
}
