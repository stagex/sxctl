package main

import (
	"flag"
	"log"
	"os"
	"path"

	"codeberg.org/stagex/sxctl/pkg/library"
	"codeberg.org/stagex/sxctl/pkg/makefile"
)

func mainGen() {
	if len(flag.Args()) < 2 {
		log.Fatal("no gen command specified")
	}

	switch flag.Arg(1) {
	case "make", "makefile", "makefiles":
		genMakefiles()
	default:
		log.Fatalf("unhandled gen command %q", flag.Arg(1))
	}
}

func genMakefiles() {
	l := library.NewRamLibrary()

	if err := library.CollectPackagesFromDirectory(baseDir, l); err != nil {
		log.Fatalf("failed to create package library from %q: %s", baseDir, err.Error())
	}

	f, err := os.Create(path.Join(baseDir, "src", "packages.mk"))
	if err != nil {
		log.Fatal("failed to create temporary file for writing:", err)
	}

	stage2, err := l.FindByName("stage2")
	if err != nil {
		log.Fatal("failed to find stage2 package:", err)
	}

	if stage2.Dependencies[0].Name != "stage1" {
		log.Fatal("bad")
	}

	defer f.Close() //nolint: errcheck

	if err := makefile.GeneratePackagesMk(f, l); err != nil {
		log.Fatal("failed to write packages.mk:", err)
	}

	log.Print("wrote packages.mk from current library contents")
}
