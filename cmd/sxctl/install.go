package main

import (
	"fmt"
	"io"
	"os"
	"path"
)

func Install(destinationDirectory string) error {
	sourceFile, err := os.Executable()
	if err != nil {
		return fmt.Errorf("failed to locate the path of the sxctl binary: %w", err)
	}

	if err := os.MkdirAll(destinationDirectory, 0o755); err != nil {
		return fmt.Errorf("failed to create receiving directory: %w", err)
	}

	out, err := os.OpenFile(path.Join(destinationDirectory, "sxctl"), os.O_CREATE|os.O_WRONLY, 0o755)
	if err != nil {
		return fmt.Errorf("failed to create output file %s: %w", path.Join(destinationDirectory, "sxctl"), err)
	}

	defer out.Close() //nolint: errcheck

	in, err := os.Open(sourceFile)
	if err != nil {
		return fmt.Errorf("failed to open source file %s: %w", sourceFile, err)
	}

	defer in.Close() //nolint: errcheck

	_, err = io.Copy(out, in)

	return err
}
